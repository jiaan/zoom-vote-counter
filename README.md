# zoom-vote-counter

This is a tiny website that counts the votes cast in zoom chat using the chat log.

https://jiaan.gitlab.io/zoom-vote-counter

## Development

### Project setup

```bash
yarn install
```

### Compiles and hot-reloads for development

```bash
yarn serve
```

### Compiles and minifies for production

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```
